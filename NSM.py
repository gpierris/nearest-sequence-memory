#Author: Georgios Pierris
#Nearest Sequence Memory which was introduced by Andrew McCalum

import random
from itertools import izip
import operator
import copy

reverse_enumerate = lambda l: izip(xrange(len(l)-1, -1, -1), reversed(l))

#My compare function to sort a 2D list based on the 
def myCMP( x, y):
	
	if(x[1] > y[1]):
		return 1
	elif(x[1] == y[1]): #If they have equal occurences, then sort by the highest discounted reward
		if(x[2] > y[2]):
			return 1
		elif(x[2] < y[2]):
			return -1
		else:
			return 0
	else:
		return -1
	


class Trace:
	

	def __init__(self, length = 0, actionSize = 2):
	
		self.length = length
		self.sequence = [0]*length
		self.actions = [[0.0]*actionSize]*length
		self.observations = [[0.0]*actionSize]*length
		self.rewards = [0]*length 
		
		self.maxReward = 1

	def addAction(self, actionID, observationVal, actionVal):
		
		self.sequence.append(actionID)
		self.actions.append(actionVal)
		self.observations.append(observationVal)
		
		self.length = len(self.sequence)
		

	def addActionAt(self, pos, actionID, observationVal, actionVal):
		
		try:
		
			self.sequence[pos] = actionID
			self.actions[pos] = actionVal
			self.observations[pos] = observationVal
			
			self.length = len(self.sequence)

		except:

			print 'Error: Could not add Action at: ', pos

	def discountReward(self):
	
		for i in range(self.length):
			self.rewards.append((0.9**(self.length - i - 1))*self.maxReward)
			
			
class TraceLibrary:

	def __init__(self, K = 10, N = 40): # n traces
		self.K = K
		self.N = N
		self.tracePool = []
		
	def addTrace(self, trace):
		
		self.tracePool.append(trace)
		
	
class NSM:

	def __init__(self, SOM, K = 10, N = 40):
		self.K = K
		self.N = N
		self.traceLib = TraceLibrary(self.K, self.N) # K = nearest sequences, n = total traces
		self.SOM = SOM
		self.maxReward = 1
		


	def printInfo(self):

		print '\n\nNearest Sequence Memory algorithm'

		print 'maximum number of actions K = ', self.K
		print 'maximum number of traces N = ', self.N

		print 'Current number of traces in library: ', len(self.traceLib.tracePool)
		print '\n\n'

	def loadTrainingData(self, filename):
		
		fin = open(filename, 'r')
		
		curTrace = Trace(0, 2)
		
		for line in fin.readlines():
			
			if('End' in line): #End of trace
				#add the old trace in the library
				curTrace.discountReward()
				self.traceLib.addTrace(curTrace)
				#create a new trace
				
				curTrace = Trace(0, 2)
				continue

			obs = line.rstrip().split(';')[0]
			act = line.rstrip().split(';')[1]
			
			observations = obs.rstrip().split(' ')
			actions = act.rstrip().split(' ')
			
			
			observations = [float(x) for x in observations]
			actions = [float(x) for x in actions]
			
			
			actionID, obs, act = self.SOM.predict(observations)
			'''
			print '------------------------'
			print observations
			print obs
			print '\n'
			print actions
			print act
	
			print '************************'
			'''
			curTrace.addAction(actionID, obs, act)
			#print actionID
	
	def runTrials(self, initialState, numOfTrials):
	
		allTrials = []
		for trial in range(numOfTrials):

			print '\nTrial', trial+1

			allTrials.append( self.simulateTrace(initialState, len(self.traceLib.tracePool[0].sequence)) )

		return allTrials
			
	def simulateTrace(self, initialState, numOfSteps):
		
		allStates = []
		#Create a new trace
		curTrace = Trace(0, 2)
	
		#Get the starting position 
		actionID, curState, act = self.getStartingPosition()
		#print actionID, curState, act

		curState = copy.deepcopy(initialState)
		curTrace.addAction(actionID, curState, act)
		
		allStates.append(copy.deepcopy(curState))
		
		# Execute Action
		for i in range(len(curState)):
			curState[i] = curState[i] + act[i]
		
		#raw_input('Press Enter to continue')
		allStates.append(copy.deepcopy(curState))
		
		for curStep in range(numOfSteps):

			actionMap = self.findSimilarOccurences(curTrace)

			actionID, obs, act = self.chooseAction(actionMap)
			
			curTrace.addAction(actionID, obs, act)
		
			#print actionID
			# Execute Action
			for i in range(len(curState)):
				curState[i] = curState[i] + act[i]
			
			#raw_input('Press Enter to continue')
			allStates.append(copy.deepcopy(curState))

			if( self.SOM.best_match(curState) == self.traceLib.tracePool[0].sequence[len(self.traceLib.tracePool[0].sequence)-1] ): #Check whether you've reached the goal
				pass#break
		

		curTrace.discountReward()	
		self.addTrace(curTrace)
	
		return allStates


	def getStartingPosition(self):
		
		randTrace = random.randint(0, len(self.traceLib.tracePool)-1)
		#print self.traceLib.tracePool[randTrace].sequence[0], self.traceLib.tracePool[randTrace].observations[0]
		return self.traceLib.tracePool[randTrace].sequence[0], self.traceLib.tracePool[randTrace].observations[0],  self.traceLib.tracePool[randTrace].actions[0]
	
		
		
	def findSimilarOccurences(self, curTrace):
	
		actionMap = []
		
		for trace in self.traceLib.tracePool:
		
			for startingIDX in range(len(trace.sequence)-2, -1, -1):
				#print curTrace.sequence, trace.sequence
				lastIDXofTrace = len(curTrace.sequence) - 1
				occurencesSum = 0.0
				reachTheBeginning = True #Reach the beginning of the trace with perfect matches
				for curIDX in range(startingIDX, -1, -1):
					
					if(lastIDXofTrace<0):
						break

					#print trace, lastIDXofTrace, curIDX
					#print len(curTrace.sequence), curTrace.sequence[lastIDXofTrace], trace.sequence[curIDX]
					

					#if(curTrace.sequence[lastIDXofTrace] == trace.sequence[curIDX] and trace.rewards[curIDX] == (0.9**( len(self.traceLib.tracePool[0].sequence)  - lastIDXofTrace - 1)) * self.maxReward):
					
					if(curTrace.sequence[lastIDXofTrace] == trace.sequence[curIDX]):
						#print 'got in'
						occurencesSum = occurencesSum + 1

					else:

						if(occurencesSum > 0.0):
							reachTheEnd = False
							actionMap.append([trace.sequence[startingIDX+1], occurencesSum, trace.rewards[startingIDX+1]])
						break
					
					
					if(reachTheBeginning and occurencesSum > 0.0):
						actionMap.append([trace.sequence[startingIDX+1], occurencesSum, trace.rewards[startingIDX+1]])
						
					lastIDXofTrace = lastIDXofTrace -1
		
		return actionMap
		
		
		
		
	def chooseAction(self, actionMap):
		
		if(actionMap == []):
			randAct = random.randint(1, self.SOM.height*self.SOM.width-1)
			return randAct, self.SOM.getObservation(randAct), self.SOM.getAction(randAct) 
		
		else:
			
			act, actionMap = self.getWinningAction(actionMap)
			

			#for curAct in actionMap:
			#	print curAct
			#print 'After'
			
			return act, self.SOM.getObservation(act), self.SOM.getAction(act) 
	
	
	def getWinningAction(self, actionMap):

		actionMap.sort(myCMP, None, True) # True, i.e., In reverse order!!!
		# sort actions based on nearness
		if(len(actionMap) > self.K):
			actionMap = actionMap[0:self.K] # Keep only the K best actions

		#Test
		#return actionMap[0][0], actionMap

		#Group equal actions to average them later
		actionDic = dict()
		for idx, val in enumerate(actionMap):
			#FIXME: I shouldn't 			
			try:
				actionDic[val[0]] = [ actionDic[val[0]][0] + 1,  actionDic[val[0]][1] + val[2]   ] #Increase occurence by one and sum reward
			except:
				actionDic[val[0]] =  [1, val[2]] #One occurence with that reward 

		for key in actionDic.iterkeys():
		    
		    actionDic[key] = actionDic[key][1] / actionDic[key][0] # Set the mean value of the action

		#print actionDic

		return max(actionDic.iteritems(), key=operator.itemgetter(1))[0], actionMap


	def writeInFile(self, allTrials):
	
		fout = open('output.dat', 'w')
		
		#print allTrials

		for trial in allTrials:
			for state in trial:
				for dim in state:
					fout.write(str(dim) + ' ')
				fout.write(';0.0 0.0 ;0 |0 0.30332 1\n')
			fout.write('Unsuccessful Trial\n')
					
		fout.close()
		

		
	#Helper function! Gets a vector and return with noise added
	def addNoise(self, inputVector, mean, sigma):
	
		for i in range(len(inputVector)):

			inputVector[i] = inputVector[i] + random.gauss(mean, sigma)
		
		return inputVector
			
		
			
		
	def reproduceCustomTrace(self, initialState, pattern):

		allStates = []
	
		for idx, actionID in enumerate(pattern):

			if(idx == 0):
				#curState = self.SOM.getObservation(pattern[0]) # Take the first observation
				curState = initialState
				allStates.append(copy.deepcopy(curState))

			

			act = self.SOM.getAction(actionID)
		
			for i in range(len(curState)):
				curState[i] = curState[i] + act[i]
			
			allStates.append(copy.deepcopy(curState))
			
			
		return [allStates]

		
	def addTrace(self, newTrace):

		if( len(self.traceLib.tracePool) >= self.N ):
			self.traceLib.tracePool.pop(0) #Pop the first which is the oldest


		self.traceLib.addTrace(newTrace)

			
			
				
			
