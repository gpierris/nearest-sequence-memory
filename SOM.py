##Author: Georgios Pierris
## Self-Organizing Map
## NO Training! Only imports weigths from trained maps coming from RLSOM

## base code from Paras Chopra
## http://paraschopra.com/sourcecode/SOM/

from random import *
from math import *

class Node:

	def __init__(self, weights_size=2, row=0, col=0):
		self.w_size = weights_size
		self.weights = [0.0]*weights_size # weights
		self.row = row
		self.col = col
		for i in range( weights_size ):
			self.weights[i] = 0*random() 
            

class SOM:

	#Let radius=False if you want to autocalculate the radius
	def __init__(self, height=10, width=10, weights_size=10):
		self.height=height
		self.width=width

		self.total=height*width

		self.nodes=[0]*(self.total)
		self.weights_size=weights_size

		for row in range(self.height):
			for col in range(self.width):
				self.nodes[(row)*(self.width)+col]=Node(weights_size, row, col)

	def loadSOM(self, filename):
	
		fout = open(filename, 'r')
	
		allLines = fout.readlines()

		for line in allLines:
			
			data = line.rstrip().split(' ')
			for i in range(len(data)):
				data[i] = float(data[i])
			
			if( int(data[0]) > 0 ):
				break	# We care only about the bottom layer
			
			#print data
			self.nodes[int(data[1])*self.width+int(data[2])].weights = data[3:-1] #Take the action, i.e., from 3 and Ignore the reward, i.e., -1

	
	def predict(self, featureVector = [0.0]):
		best = self.best_match(featureVector)
		return best, self.nodes[best].weights[0:2], self.nodes[best].weights[2:4]
        
	def best_match(self, target_FV=[0.0]):

		minimum = 1000
		minimum_index=1 #Minimum distance unit
		temp=0.0
		for i in range(self.total):
			temp=0.0
			temp=self.FV_distance(self.nodes[i].weights,target_FV)
			if temp<minimum:
				minimum=temp
				minimum_index=i

		return minimum_index

	def FV_distance(self, FV_1=[0.0], FV_2=[0.0]):
		temp=0.0
		for j in range(self.weights_size):
			temp=temp+(FV_1[j]-FV_2[j])**2

		temp=sqrt(temp)
		return temp
	
	def getAction(self, actionID):
		return self.nodes[actionID].weights[2:4]
		#return self.nodes[actionID/self.width + actionID%self.height].weights[2:4]
	
	def getObservation(self, actionID):
		return self.nodes[actionID].weights[0:2]
		#return self.nodes[actionID/self.width + actionID%self.height].weights[0:2]
	
		
		

