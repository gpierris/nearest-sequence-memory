import sys
#Author: Georgios Pierris

import SOM
import NSM



# Load the self organizing map
som = SOM.SOM(8,8,2)
som.loadSOM('HSOM.dat')


#Prepare and load the traces from the training data
nsm = NSM.NSM(som, 30, 20)
nsm.loadTrainingData('trainingDataCornu2012-02-18 18:58:58.052396.dat')

nsm.printInfo()

for trace in nsm.traceLib.tracePool:
	print trace.sequence
	
#nsm.writeInFile(nsm.reproduceCustomTrace([-0.31397090698, 0.32898681337], [7, 7, 7, 7, 15, 15, 15, 15, 23, 22, 14, 14, 21, 21, 13, 5, 4, 4, 12, 12, 3, 2, 11, 11, 10, 19, 19, 18, 18, 26, 26, 26, 34, 33, 33, 33, 40, 48, 48, 56, 56, 57, 57, 58, 58, 58, 59, 59, 60, 60, 60, 61, 61, 61, 62, 62, 62, 63, 55, 55, 47, 47, 47, 39, 38, 38, 38, 37, 37, 36, 36, 36, 36, 43, 43, 42, 42, 41, 40, 32, 24, 24, 16, 16, 16, 16, 8, 8, 0, 0, 0]))

#sys.exit()

#Simulate from a starting position 
trial = nsm.runTrials([-0.31397090698, 0.32898681337], 20) # the number of trials
nsm.writeInFile(trial)

nsm.printInfo()

#print nsm.traceLib.tracePool
#print nsm.traceLib.tracePool[0].sequence
#print nsm.traceLib.tracePool[1].sequence
#print nsm.traceLib.tracePool[2].sequence
#print nsm.traceLib.tracePool[3].sequence
#print nsm.traceLib.tracePool[4].sequence
#print nsm.traceLib.tracePool[5].sequence
