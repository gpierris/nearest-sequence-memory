import sys
import time
import random

try:
	import pylab
	import numpy as np
	import matplotlib.pyplot as plt
	from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization! 

except:
	print 'Plot is not supported'
	sys.exit()


global fig
global labels
global ax
global toDelete

ax = []
#labels = ['LShoulderPitch', 'LShoulderRoll', 'LElbowYaw', 'LElbowRoll', 'Reward']
labels = ['X', 'Y', 'Historical Match', 'Reward', 'Y over X']

def setPlotDimensions(fileName):

	temp = open(fileName, 'r')

	line = temp.readline()
	temp.close()

	line = line.split(';')

	return [len(line[0].rstrip().split(' ')), len(line[1].rstrip().split(' '))] #Number of observation and number of action




def plot2D(fileName, dim, resultPlotting = False):

	global fig
	global labels
	global ax
	global toDelete

	try:
		fig.hold(True)
	except:
		print 'No figure yet... Creating one now!'
		fig = plt.figure()
		fig.suptitle('Joint Trajectories', fontsize=12)

		fig.hold(True)

	plotPoints = []
	for i in range(dim[0]+1):	# +1 for the reward	
		plotPoints.append([])
	
	pylab.ion()

	fout = open(fileName, 'r')

	countTrials = 0
	action = []
	allCntx = []
	allIM = []
	allAP = []

	print dim[0]

	for i in range(dim[0]+3):
		ax.append(fig.add_subplot(dim[0]+3,1,i+1)) # +1 for the reward
		
	allbars = []
	for line in fout.readlines():
		try:
			stats = line.rstrip().split("|")[1]
			line = line.rstrip().split("|")[0]
		except:
			stats = ''
			pass # This exception raised because there was no | on the line		


		if ('End' in line) or ('Unsuccessful Trial' in line) or ('Reward received - End of trial' in line):
			countTrials = countTrials + 1

			for i in range(6):
				if(resultPlotting and len(ax[i].lines) >= 2):
				
					try:
						del ax[i].lines[-1]
					except:
						print 'adfad', i
						print 'No Line'

			if('Reward received - End of trial' in line):
				par = '^--'
				colour = 'green'
			elif( 'End' in line):
				par = ''
				colour = [random.random(), random.random(), random.random()]
			elif('Unsuccessful Trial' in line):
				par = '*--'
				colour = 'red'

			
			for i in allbars:
				for j in i:
					j.set_visible(False)



			for i in range(dim[0]+3):

				plt.subplot(dim[0]+3,1,i+1)
				plt.ylabel(labels[i])

				#plt.set_autoscale_on(False)

		
				if(i < 2 or i == 3):
					plt.plot(np.array(plotPoints[i - i/3]), par, c=colour)
					pylab.xlim([-1, len(allCntx)])
			
				elif(i==4):
					plt.plot(np.array(plotPoints[0]), np.array(plotPoints[1]), par, c=colour)
					plt.axis('equal')

				elif(i==2):

					width = 0.25
					x1 = np.array(range(len(allCntx))) - (3/2.0) *width
					x2 = np.array(range(len(allIM))) - (1/2.0)*width
					x3 = np.array(range(len(allAP))) + (1/2.0)*width

					allbars.append(plt.bar(x1, allCntx, width = width, color='b'))
					allbars.append(plt.bar(x2, allIM, width = width, color='r'))
					allbars.append(plt.bar(x3, allAP, width = width, color='g'))
					pylab.xlim([-1, len(allCntx)])
				
				#plt.yaxis.grid(True) 
				plt.grid(True, which = 'major')

			
			
			arrows = []
			try:	
				for i in toDelete:
					i.set_visible(False)
			except:
				toDelete = []


			for y in range(len(action[0])):	#for every diagram

				curax = plt.subplot(dim[0]+3,1,y+1)
				for x in range(len(plotPoints[0])):
					arrows.append(plt.Arrow(x-1, plotPoints[y][x]+cmp(float(action[x][y]) , 0)*0.001, 0, float(action[x][y]), width=0.2))
				
				for arrow in arrows:
					toDelete.append(curax.add_patch(arrow))

				arrows = []
				
								

			'''
			'''
			plt.show()
			try:			
				raw_input('Press Enter to Continue...')
			except KeyboardInterrupt:
				raw_input('Press Enter to Continue...')

			
			for i in range(len(observation)+1):
				plotPoints[i] = []
				plotPoints[i] = []
			action = []
			allCntx = []
			allIM = []
			allAP = []

				
		
		else:

			sections = line.split(';')
			observation = sections[0].rstrip().split(' ')
			action.append(sections[1].rstrip().split(' '))
			reward = sections[2].rstrip().split(' ')

			if(stats != ''):
				stats = stats.lstrip().split(" ")
				allCntx.append(float(stats[0]))
				allIM.append(float(stats[1])) 
				allAP.append(float(stats[2])) 
			else:
				allCntx.append(0.0)
				allIM.append(0.0) 
				allAP.append(0.0)

			for i in range(len(observation)):
				plotPoints[i].append( float(observation[i]) )

			plotPoints[-1].append(float(reward[0]))
	plt.show()



def main():

	try:
		fileTraining = sys.argv[1]
		fileOutput = sys.argv[2]
	except:
		print 'Wrong number of arguments'
		sys.exit(1)

	if(len(sys.argv) > 4):
		print 'No ploting. Saving images instead'
		title = sys.argv[4]
		print title
	else:
		title = ''

	dim = setPlotDimensions(fileTraining)
	plot2D(fileTraining, dim)
	#raw_input("Press enter to continue!")

	if(title == ''):
		pass#raw_input("Please press enter to continue plotting the results!")

	#plt.draw()
	plot2D(fileOutput, dim, True)

	if(title != ''):
		plt.savefig(title, format = 'png')
		
	else:


		try:
			plt.show()
		except KeyboardInterrupt:
			sys.exit()
		
main()
raw_input('Press Enter to exit!')


